#!/usr/bin/env python

# System
import sys
import time

# Processes/Threads
import pyclone

# Data
import re

# Logging
import tqdm

# Create an instance using rclone with defaults (e.g. rclone at host level, no STDERR redirect, and a minimal message buffer size.)
rclone	=	pyclone.PyClone()

# Copy files from local to remote
rclone.copy(
	source	=	'/mnt/familyPhotos',
	remote	=	'googleDrive',
	path	=	'/backups/familyPhotos'
)

try:

	# Overall progress
	totalProgress	=	tqdm.tqdm( leave=False, unit='pct', unit_scale=False, total=100 )
	totalProgress.set_description( 'Total transfer' )

	# Dictionary of progress bars
	pbars			=	[]

	# Read message buffer
	while rclone.tailing():

		# Buffer contents found
		if rclone.readline():

			# Transfer activity found
			transfers		=	rclone.line.get( 'stats', {} ).get( 'transferring', [] )
			transfersCount	=	len( transfers )

			# Update count of progress bars
			pbarsCount	=	len( pbars )

			# Update total progress
			statusMatch	=	None
			if rclone.line.get( 'msg' ):

				# Parse status message
				statusMatch		=	re.search( '.*Transferred:[\s]+.*[\s]+([0-9]+)%.*[\s]+ETA[\s]+.*', rclone.line[ 'msg' ] )

				# Parse match found
				if statusMatch:

					# Update total percentage
					totalProgress.n		=	int( statusMatch.group( 1 ) )
					totalProgress.update()

					pass # END IF : MATCH

				pass # END IF : MESSAGE
			del statusMatch

			# Add progress bars
			if transfersCount > pbarsCount:
				pbars.extend([ tqdm.tqdm( leave=False, unit='b', unit_scale=True ) for i in range( transfersCount - pbarsCount ) ])
				pass # END IF : ADD PROGRESS BARS

			# Iterate list of transfers and add a reciprocal index
			for i, t in enumerate( transfers ):

				# Update percentage and name for current progress bar
				pbars[ i ].total	=	t[ 'size' ]
				pbars[ i ].n		=	t[ 'bytes' ]
				pbars[ i ].set_description( t[ "name" ] )
				pbars[ i ].update()

				pass # END FOR

			# Remove progress bars
			if transfersCount < pbarsCount:
				[ pb.close() for pb in pbars[ ( pbarsCount - transfersCount ): ] ]
				del pbars[ ( pbarsCount - transfersCount ): ]
				pass # END ELIF : REMOVE PROGRESS BARS

			pass # END IF : READ LINE

		# Wait 0.5 seconds until we query message buffer again
		time.sleep( 0.5 )

		pass # END WHILE LOOP

	# Refresh total progress
	totalProgress.refresh()

	pass # END TRY

except KeyboardInterrupt:
	print()
	pass # END KEYBOARD INTERRUPT

finally:

	# Remove any remaining progress bars
	[ pb.close() for pb in pbars[ ( pbarsCount - transfersCount ): ] ]
	totalProgress.close()
	del totalProgress, pbars

	# Clean-up
	rclone.stop()

	pass # END FINALLY
