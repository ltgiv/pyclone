#!/usr/bin/env python

# System
import sys
import time

# Data
import ruamel.yaml

# Processes/Threads
import pyclone

# Create an instance using rclone with defaults (e.g. rclone at host level, no STDERR redirect, and a minimal message buffer size.)
rclone	=	pyclone.PyClone()

# Copy files from local to remote
rclone.copy(
	source	=	'/mnt/familyPhotos',
	remote	=	'googleDrive',
	path	=	'/backups/familyPhotos'
)

try:

	# Read message buffer
	while rclone.tailing():

		# Buffer contents found
		if rclone.readline():

			# Dump output into YAML
			print( ruamel.yaml.dump( rclone.line, sys.stdout, Dumper=ruamel.yaml.RoundTripDumper ), flush=True )

			pass # END IF : READ LINE

		# Wait 0.5 seconds until we query message buffer again
		time.sleep( 0.5 )

		pass # END WHILE LOOP

	pass # END TRY

except KeyboardInterrupt:
	print()
	pass # END KEYBOARD INTERRUPT

finally:

	# Clean-up
	rclone.stop()

	pass # END FINALLY
