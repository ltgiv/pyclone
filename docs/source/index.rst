.. PyClone documentation master file, created by
   sphinx-quickstart on Tue Sep 22 21:55:52 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PyClone's documentation!
===================================

PyClone wraps `rclone <https://rclone.org/>`_ and provides an interface for an installation at the host or container level.

.. automodule:: pyclone
    :members:
    :show-inheritance:
    :undoc-members:
    :private-members:
    :special-members: __init__

References
==================

* `Introduction <https://gitlab.com/ltgiv/pyclone/blob/master/README.md>`_

* `Package <https://pypi.org/project/PyClone/>`_

* `Source <https://gitlab.com/ltgiv/pyclone>`_

* `Tracker <https://gitlab.com/ltgiv/pyclone/issues>`_

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   PyClone API <pyclone/pyclone>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
